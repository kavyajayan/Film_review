Rails.application.routes.draw do
  devise_scope :user do
    get 'users/sign_out', to: 'devise/sessions#destroy'
  end
  devise_for :users, controllers: { sessions: 'users/sessions' }
  get '/admin', to: 'movies#edit', as: 'user_root'
  get '/movies/:id', to: 'movies#rate', as: 'movies_rate'
  post '/movies/:movie_id/comments', to: 'comments#create'
  delete '/movies/:movie_id/comments/:id', to: 'comments#destroy'
  get '/movies/:movie_id/comments', to: 'comments#index', as: 'movie_comments'
  get '/movies_update/:id', to: 'movies#show_update', as: 'movie'
  patch '/movies_update/:id', to: 'movies#update'
  put '/movies_update/:id', to: 'movies#update'
  delete '/movies_edit/:id', to: 'movies#destroy'
  get '/movies_new', to: 'movies#new', as: 'movies_new'
  post '/movies_new', to: 'movies#create', as: 'movies'
  delete '/movies_update/:id', to: 'movies#destroy'

  root 'movies#show'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
