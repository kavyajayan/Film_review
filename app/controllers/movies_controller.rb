class MoviesController < ApplicationController
  def show
		@movies=Movie.all
		@movies=@movies.sort_by{|a| a.title}
	end

	def new
    @movie = Movie.new
  end

	def create
    @movie = Movie.create(movie_params)
    redirect_to user_root_path
  end
	
	def edit
		@movies=Movie.all
		@movies=@movies.sort_by{|a| a.title}
	end

	def show_update
		@movie = Movie.find(params[:id])
	end

	def update
    @movie = Movie.find(params[:id])
    if @movie.update(movie_params)
      redirect_to @movie
    else
      render 'show_update'
    end
  end

	def rate
		@movie= Movie.find(params[:id])
		puts(@movie)
	end

	def destroy
		@movie=Movie.find(params[:id])
    @movie.destroy
    redirect_to user_root_path
  end
	 
  private
    def movie_params
      params.require(:movie).permit(:title, :year, :category)
    end
end
